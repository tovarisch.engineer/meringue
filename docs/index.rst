.. .. Meringue documentation master file, created by
..    sphinx-quickstart on Wed Jun 24 13:37:51 2015.
..    You can adapt this file completely to your liking, but it should at least
..    contain the root `toctree` directive.

Добро пожаловать, Путник! Это документация Meringue.
====================================================

Meringue это пакет с набором готового функицонала для `Django <http://www.djangoproject.com>`_, она включает в себя такие вещи как миксины для моделей, абстрактные модели, утилиты для работы с изображениями, набор декораторов, набор кастомных полей и прочее, подробне можно ознакомиться в этой документации.



Поддержка
---------

Модуль разрабатывается одной очень маленькой командой, исключительно под личные нужды, при необходимости можно ознакомиться со `списком задач <http://code.weboven.net/weboven_team/meringue/issues>`_, а если возникли вопросы или предложения всегда можно написать на почту - dd@weboven.net.

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. To get up and running quickly, consult the :ref:`quick-start guide
.. <quickstart>`, which describes all the necessary steps to install
.. django-admin-tools and configure it for the default setup.
.. For more detailed information about how to install and how to customize
.. django-admin-tools, read through the documentation listed below.

Содержание:
-----------

.. toctree::
   :maxdepth: 2

   quickstart
   configuration

.. installation
.. customization
.. multiple_admin_sites
.. menu
.. dashboard
.. integration
.. contributing
.. testing